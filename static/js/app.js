$(function() {
    var limit = 20;
    var start = 0;  
    var locked = false;
    window.furthest = 0;
/*    $(window).scroll(function() {
#        scrollTop = $(window).scrollTop();
#        if(scrollTop > window.furthest) {
            window.furthest = scrollTop;
            populate();

        }
    });*/


    function populate() {
        stopLoading();
        if(!locked) {
            locked = true;
            size = parseInt(($(window).width() - 14) / $('#nums').val());
            path = $('#path').val().replace(/\//g, ':');
            $.getJSON("/list/"+path+'/'+start+'/'+limit, function(data) {
                if(data.status != 204 && data.status != 404) {                    
                    $.each(data, function(i, d) {
                        locked = true;
                        $.getJSON('/test'+d.thumb+'/'+size, function(data) {
                            if(data.result == 200) {
                                $('#images').append('<a href="'+d.url+'"><img src="'+d.thumb+'/'+size+'" height="'+size+'px"/></a>');
                            }
                        });
                        
                    });
                    startLoading();
                } else {
                    stopLoading();
                }
                locked = false;
                });
                start = start+limit;
            locked = false;
        }
    }

    function scrollTest() {
        if($(document).height() < $(window).scrollTop() + $(window).height() + 400) {
            populate();
        };
    }
    function reset() {
        $('#images').empty();
        start = 0;
        locked = false;
        startLoading();
    }


    var refresh;

    function startLoading() {
        refresh = setInterval(scrollTest,100);
    }

    function stopLoading() {
        clearInterval(refresh);
    }

    $('#images a').live('click', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        $('#full').empty();
        $('#full').css('top', $(window).scrollTop());
        $('#full').append(
            '<img src="'+url+'"/>'
        );
        img = $('#full img');
        img.css('height', $(window).height()+'px'); 
        $('#full').fadeIn();
    });

    $('#full img, #full').live('click', function() {
        $('#full').fadeOut();
    });

    $('#nums').change(function() {
        reset();
        populate();    
    });
    $('#path').change(function(e) {
        reset();
        e.preventDefault(); 
        populate();  
    });
    $('#go').click(function(e) {
        reset();
        e.preventDefault(); 
        populate();  
    });
    $('#formm').submit(function(e) {
        reset();
        e.preventDefault();
        populate();  
    })
    reset();
    populate(); 
});