# Pretty, a flask app and frontend for browsing shitloads of images in a directory.
# For porn.
from flask import Flask, request, g, url_for, abort, \
     render_template, flash, Response
from meinheld import server
import json
from sources import LocalSource, RemoteSource, PathError


DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def index():
    return render_template('main.html')

@app.route('/list/<string:path>/<int:start>/<int:limit>')
def list(path, start, limit):
    try:
        if path[:4] == 'http':
            s = RemoteSource(path=path) 
        else:
            s = LocalSource(path=path)
        images = s.images
    except PathError:
        return json.dumps({
            'status': 404
        })
    if start > len(images):
        return json.dumps({
            'status': 204
        })
    else:
        return json.dumps(images[start:start+limit])


@app.route('/thumb/<string:path>/<int:size>')
def thumb(path, size):
    try:
        s = LocalSource(path=path)
        data, mime = s.get_thumb(path, size)
        r = Response(response=data,content_type=mime)
    except PathError:
        pass
    return r

@app.route('/full/<string:path>')
def full(path):
    try:
        s = LocalSource(path=path)
        data, mime = s.get_full(path)
        r = Response(response=data,content_type=mime)
    except PathError:
        pass
    return r

@app.route('/test/thumb/<string:path>/<int:size>')
def test(path, size):
    try:
        s = LocalSource(path=path)
        data, mime = s.get_thumb(path, size)
        return json.dumps({
            'result': 200
        })
    except (PathError, IOError):
        return json.dumps({
            'result': 404
        })
    return r
    


if __name__ == "__main__":
#    server.listen(("0.0.0.0", 5000))
#    server.run(app)
    app.run()