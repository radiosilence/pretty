import os
import redis
import Image, ImageOps
import urllib2

class Source(object):
    default_types = [
        'gif',
        'jpg',
        'jpeg',
        'png'
    ]
    redis = None
    thumb_sizes = [
        960, 640, 480, 320, 240, 192, 96
    ]

    @property
    def _r(self):
        if not self.redis:
            self.redis = redis.Redis(
                host='localhost',
                port=6379,
                db=13
            )
        return self.redis
    def sort_images(self):
        self._images.sort(reverse=self.sortby[1], key=lambda x: x[self.sortby[0]])
    
    def _decode_path(self, path):
        return path.replace(':', '/')
    
    def _encode_path(self, path):
        return path.replace('/', ':')

class PathError(Exception):
    pass

class LocalSource(Source):
    def __init__(self, path=None, sortby=('mtime', True), recursive=False):
        self._images = []
        if not path:
            path = '.'
        self.sortby = sortby
        self.path = self._decode_path(path)
        self._test_path()

    def _test_path(self):
        if not os.path.exists(self.path):
            if os.path.exists(urllib2.quote(self.path)):
                self.path = urllib2.quote(self.path)
            elif os.path.exists(urllib2.unquote(self.path)):
                self.path = urllib2.unquote(self.path)
            else:
                raise PathError('Does not exist %s' % self.path)


    @property
    def images(self):
        items = filter(lambda f: f.split('.')[-1].lower() in self.default_types, os.listdir(self.path))
        for item in items:
            filename = '%s/%s' % (self.path, item)
            self._images.append({
                'url': '/full/%s' % self._encode_path(filename),
                'thumb': '/thumb/%s' % self._encode_path(filename),
                'mtime': os.stat(filename).st_mtime
            });
        
        self.sort_images()
        return self._images

    def _best_thumb(self, size):
        last_size = self.thumb_sizes[0]
        for s in sorted(self.thumb_sizes, reverse=True):
            if s < size:
                break;
            last_size = s
        return last_size
            
    def get_thumb(self, filename, size):
        self.path = self._decode_path(filename)
        print "Getting thumb", self.path, size
        self._test_path()
        ext = self.path.split('.')[-1]
        size = self._best_thumb(size)
        f = self._get_thumb_data(self.path, size)
        return f, 'image/%s' % ext.lower

    def get_full(self, filename):
        self.path = self._decode_path(filename)
        self._test_path()
        ext = self.path.split('.')[-1]
        f = open(self.path, 'r').read()
        return f, 'image/%s' % ext.lower()

    def _get_thumb_data(self, filename, size):
        key = "image:%s:%s" % (filename, size)
        if not self._r.exists(key):        
            t = MemFile()
            im =  Image.open(filename)
            im = ImageOps.fit(im, (size, size), Image.ANTIALIAS, 0, (0.5, 0.5))
            im.save(t, 'PNG')
            thumb = t.read()
            self._r.set(key, thumb)
        else:
            thumb = self._r.get(key)
        return thumb

class MemFile(object):
    data = ''
    def write(self, data):
        self.data += data

    def read(self):
        return self.data
class RemoteSource(Source):
    pass